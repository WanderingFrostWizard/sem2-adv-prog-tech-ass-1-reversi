/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #1 
 * Full Name        : Cameron Watt
 * Student Number   : s3589163
 * Course Code      : COSC1076 - Advanced Programming Techniques
 * Program Code     : BP215 - Games and Graphics Programming
 * Start up code provided by Paul Miller 
 **********************************************************************/
#include <time.h>
#include "shared.h"
#include "player.h"
#include "gameboard.h"

#ifndef GAME_H
#define GAME_H

struct player * play_game(struct player * human, struct player * computer);
BOOLEAN apply_move(game_board board, unsigned y, unsigned x,
                   enum cell player_token);
unsigned game_score(game_board board, enum cell player_token);
void swap_players(struct player ** first, struct player ** second);

void setDirection( enum direction dir, int * xMovement, int * yMovement );

#endif /* ifndef GAME_H */

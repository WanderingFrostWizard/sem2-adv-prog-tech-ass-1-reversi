/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #1 
 * Full Name        : Cameron Watt
 * Student Number   : s3589163
 * Course Code      : COSC1076 - Advanced Programming Techniques
 * Program Code     : BP215 - Games and Graphics Programming
 * Start up code provided by Paul Miller 
 **********************************************************************/
#include "gameboard.h"
#include "player.h"

/**
 * initialise the game board to be consistent with the screenshot provided
 * in your assignment specification. 
 *
 * All squares should be initialised to be blank other than the four
 * squares in the middle of the board. In that case they are initialised to 
 * appropriate colored tokens
 **/
void init_game_board(game_board board)
{
   int x, y;

   for (y = 0; y < BOARD_HEIGHT; y++)
   {
      for (x = 0; x < BOARD_WIDTH; x++)
      {
         board[x][y] = BLANK;   
      }
   }

   /* Set starting squares at (4,4), (4,5), (5,4), (5,5) */
   board[3][3] = RED;
   board[3][4] = BLUE;
   board[4][3] = BLUE;
   board[4][4] = RED;
}

/**
 * display the board as specified in the assignment specification. You should 
 * make every effort to create a board consistent with the screenshots in the 
 * assignment specification. 
 **/
void display_board(game_board board, struct player * first,
                   struct player * second)
{
   /* Current position of the code within the gameboard */
   int x = 0;
   int y = 0;       

   /* ---=== Display the Scoreboard ===--- */
   /* Top Row of the Scoreboard */
   repeatCharacter( '=', LINELEN );        

   printPlayerScores ( first, "one" );
   printPlayerScores ( second, "two" );
   printf ( "\n" );

   /* ---=== Display the Gameboard ===--- */
   /* Print the labels across the top of the gameboard */
   printXLabels( x );     

   /* Print out the divider at the top of the gameboard */
   repeatCharacter( '=', BOARD_DIVIDER_WIDTH );     

   for ( y = 0; y < BOARD_HEIGHT; y++ )        
   {
      /* Print the labels across the top of the gameboard */
      printCellValues( board, x, y );   

      /* Print out the bottom of the gameboard */
      repeatCharacter( '-', BOARD_DIVIDER_WIDTH ); 
   }

   /* Print out the bottom row of the gameboard */
   repeatCharacter( '=', BOARD_DIVIDER_WIDTH );       
   printf ( "\n" );

}

/* Is used as a generic function to print both players scores to the screen 
 * using just one function. To do this it accepts a generic pointer to the 
 * player to be printed, and a character array which will contain the players
 * number as a string ( not ideal, but ran out of ideas... ) */
void printPlayerScores ( struct player* current, char numberText[4] )
{
   /* Set player details to players token color */
   if (current->token == RED)
   {
      printRed();
   }
   else if (current->token == BLUE)
   {
      printBlue();
   }
   printf ( "Player %s's Details \n", numberText ); 
   printf ( "-------------------- \n" );
   printf ( "Name: %s        Score: %d \n", current->name, current->score );

   /* Reset the color */
   printReset();

   /* Separating Row of the Scoreboard */
   repeatCharacter( '-', LINELEN );  
}


/* Print the labels across the top of the gameboard */
void printXLabels ( int x )
{
   /* Adds a BLANK square to the top LEFT corner of the gameboard */
   printf ( "   |" );      

   for ( x = 0; x < BOARD_WIDTH; x++ )
   {
      /* Adds the Horizontal Labels - Starting from 1 to 8 */
      printf ( " %d |", x + 1 );      
   }
   printf ( "\n");
}

void printCellValues ( game_board board, int x, int y )
{
   /* Adds the Vertical Label to this column (Adjusted to show values of 1-8)*/
   printf ( " %d |" , y + 1 );         

   for (x = 0; x < BOARD_WIDTH; x++) 
   {
      if ( board[x][y] == BLANK )     /* BLANK CELLS */
      {
         printf("   |");
      }
      else if ( board[x][y] == RED )    /* RED CELLS */
      {
         printf( "%s 0 %s|", COLOR_RED, COLOR_RESET );
      }
      else if ( board[x][y] == BLUE )    /* BLUE CELLS */
      {
         printf( "%s 0 %s|", COLOR_BLUE, COLOR_RESET );
      }
   }
   printf ( "\n");
}
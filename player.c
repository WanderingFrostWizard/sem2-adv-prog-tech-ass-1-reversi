/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #1 
 * Full Name        : Cameron Watt
 * Student Number   : s3589163
 * Course Code      : COSC1076 - Advanced Programming Techniques
 * Program Code     : BP215 - Games and Graphics Programming
 * Start up code provided by Paul Miller 
 **********************************************************************/
#include "player.h"
#include "game.h"

/**
 * These two functions initialise the player structure at the beginning of the 
 * game. In both cases, you should initialise the whole structure to known safe
 * values and then prompt the user for their name. In the init_first_player() 
 * function you should then select a color at random for this player using the 
 * rand() function. This token should be assigned to the first player's token and
 * also returned via the token pointer. In init_second_player() you should just
 * test the value of token and assign the opposite color to the second player.
 **/
BOOLEAN init_first_player(struct player* first, enum cell* token)
{
   /* Temporarily contains the player name */
   char input[NAMELEN];

   /* Initialize the values */
   strcpy(first->name, "");             
   first->token = BLANK;                     
   first->score = 0;                     

   /* Randomly picks between 0,1,2 As two seems to be very rare, it is inluded
    * with 1 */
   if ( rand() % NUM_COLORS == 0 )
   {
      *token = RED;
   }
   /* If rand returned 1 or 2 */
   else 
   {
      *token = BLUE;
   }

   /* Assign the random token to the player struct */
   first->token = *token;

   /* Get first players name */
   printf( "\n" );
   printf( "Please enter the First Player's Name: " );

   /* Get the players name, or quit if the user presses enter or CTRL + D */
   if ( getName(input, NAMELEN) == FALSE )
   {
      return FALSE;
   }

   /* Assign name to the first player struct */
   strcpy(first->name, input); 

   return TRUE;                            
}


BOOLEAN init_second_player(struct player* second, enum cell token)
{
   /* Temporarily contains the player name */
   char input[NAMELEN];

   /* Initialize the values */
   strcpy(second->name, "");          
   second->token = BLANK;              
   second->score = 0;                    

   /* Assign the opposite token to player two */
   if (token == RED)
   {
      second->token = BLUE;     
   }   
   else if (token == BLUE)
   {
      second->token = RED;     
   }

   /* Get second players name */
   printf ( "Please enter the Second Player's Name: " );

   /* Get the players name, or quit if the user presses enter or CTRL + D */
   if ( getName(input, NAMELEN) == FALSE )
   {
      return FALSE;
   }  

   /* Assign name to the second player struct */
   strcpy(second->name, input);   

   return TRUE;
}



/**
 * prompts the user for a comma-separate pair of coordinates for a move in the
 * game. Each move, if valid, must capture at least one enemy piece by 
 * surrounding a sequence of one or more enemy pieces with two pieces of our 
 * own: one being the new piece to be placed and the other a piece already 
 * on the board. This function then validates that a valid move has been entered
 * calls the apply_move function to do the actual work of capturing pieces.
 **/
BOOLEAN make_move(struct player * current, game_board board)
{
   /* Temporarily stores each token, ready for conversion to integer */
   char * strToken = NULL;     
   int coorX;  /* Temporarily stores the X coordinate point */
   int coorY;  /* Temporarily stores the Y coordinate point */

   /* Used to represent if the user entered move was valid */
   BOOLEAN validMove = FALSE;

   /* Temporarily contains the player name */
   char input[NAMELEN];

   /* Loops until a valid set of coordinates are received */
   while( validMove == FALSE )
   {
      /* Clear the Input variable if the loop is repeated */
      input[0] = '0';

      printf ( "Please enter x and y coordinates separated by a comma for " );
      printf ( "the piece you wish to place: \n" );

      /* User wants to end the game */
      if ( fgets( input, 3 + EXTRACHARS, stdin ) == NULL || input[0] == '\n' )
      {
         return FALSE;
      }
      else if ( input[ 3 ] == '\0') 
      {
         printf ( " Invalid Input Detected \n" );
      }
      /* Check to see if the user has entered MORE THAN 3 characters */
      else if ( input[ 3 ] != '\n')    
      {
         printf ( " Invalid Input Detected \n" );
         read_rest_of_line();  /* Clear the rest of the buffer */
      }
      /* Stops the user from entering comma's in the x or y coordinates which
       * will cause a segmentation fault with the string tokenizer below */
      else if ( input[0] == ','  || input[2] == ',' )
      {
         printf ( " Invalid Input Detected \n" );
      }
      else if ( input[1] != ',' )
      {
         printf ( "COORDINATES MUST BE SEPARATED BY A COMMA \n" );
      }
      else 
      {
         /* Take the first coordinate upto the comma */
         strToken = strtok( input, "," );          
         coorX = (int) strtol( strToken, NULL, 10 );

         /* Take in next token from the input stream */
         strToken = strtok( NULL, " " );           
         coorY = (int) strtol( strToken, NULL, 10 );

         /* Non-Numeric Characters entered */
         if ( coorX == 0 || coorY == 0 )
         {
            printf ( " Invalid Input Detected \n" );
         }
         /* Numbers cannot be LESS than 0 */
         else if ( coorX < 0 || coorY < 0 )
         {
           printf ( " Invalid Input Detected \n" );
         }
         /* Numbers cannot be greater than BOARD_WIDTH */
         else if ( coorX > BOARD_WIDTH || coorY > BOARD_HEIGHT  )
         {
            printf ( " Invalid Input Detected \n" );
         }
         else
         {
            /* NOTE each coordinate needs to be reduced by one to match the 
             * 2D board array */
            validMove = apply_move( board, coorY - 1, coorX - 1, 
                                                current->token );
         }
      }
   }  /* End of Coordinate While loop */
   
   return TRUE; 
}


/* receives input from the user (upto the LengthOfString) and stores it in the
 * input array before returning it to the calling function */
BOOLEAN getName( char *input, int LengthOfString )
{
   /* Get the name from the player or exit using CTRL+D or NULL */  
   if ( fgets( input, LengthOfString + EXTRACHARS, stdin ) == NULL )
   {
      return FALSE;
   }   
   /* User presses enter */
   else if ( input[0] == '\n' )
   {
      return FALSE;
   }
   /* Check to see if the user has entered MORE THAN LengthOfString */
   else if( input[ strlen( input ) - 1 ] != '\n')    
   {
      printf ( " Invalid Input Detected \n" );
      /* Clear the rest of the buffer */
      read_rest_of_line();  
      return FALSE;                  
   }
   /* Name successfully Entered */
   else 
   {
      /* Replace the Newline character with a null character */
      input[ strlen( input ) - 1 ] = '\0';

      /* The players name is returned through the pointer */
      return TRUE;
   }
}



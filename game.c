/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #1 
 * Full Name        : Cameron Watt
 * Student Number   : s3589163
 * Course Code      : COSC1076 - Advanced Programming Techniques
 * Program Code     : BP215 - Games and Graphics Programming
 * Start up code provided by Paul Miller 
 **********************************************************************/
#include "game.h"

/**
 * The heart of the game itself. You should do ALL initialisation required 
 * for the game in here or call function required for that purpose. For example
 * both players should be initialised from here, you should seed the random 
 * number generator, that kind of thing. 
 *
 * Next, you should set the initial player - the initial player is the player 
 * whose token was set by initialisation to RED. 
 *
 * Next, you will need to manage the game loop in here. In each loop of the 
 * game, you should display the game board, including player scores 
 * display whose turn it is, etc in the format specified in the assignment
 * specification. Finally at the end of each turn you will calculate the score 
 * for each player you will calculate their score and store it and then you 
 * will need to swap the current player and other player 
 * using the swap_player() function. 
 * 
 * A game will end when either player presses enter or ctrl-d on a newline. 
 * 
 * When you detect a request to end the game, you should do a final calculation
 * of the scores for the two players and return the player with the highest
 * score.
 **/
struct player * play_game(struct player * first, struct player * second)
{
   /* declare a new 2D array to contain the game_board, called board */
   game_board board;

   /* Declare a variable to compare the color of player 1 to player 2 */
   enum cell token;

   /* Create pointers to the current player, other player and the winning 
      player, used throughout the game */
   struct player * current, *other, *winner; 

   /* Variable called from the time.h file to seed the rand() function */
   time_t timeSeed;

   /* Initialize the gameboard */
   init_game_board( board );

   /* Seed the random number generator */
   srand( (unsigned) time(&timeSeed) );

   /* Initialize both players, assigning a random token to player 1. If a 
    * player wishes to quit the game, return a NULL winner to main function */ 
   if ( init_first_player( first, &token) == FALSE || 
            init_second_player( second, token) == FALSE )
   {
      winner = NULL;
      return winner;
   }   

   /* Set Player one to be the current player */
   current = first;
   other = second;

   /* Red always starts first, if current player isn't red, swap players */
   if ( current->token != RED )
   {
      swap_players( &current, &other );
   }

   /* The core of the game itself, loops after every turn */
   while ( 1 )
   {
      /* Display the gameboard */
      display_board( board, first, second );

      /* Colour the following text according to the current players color */
      if ( current->token == RED )
      {
         printRed();
      }
      else if ( current->token == BLUE )
      {
         printBlue();
      }

      printf( "It's %s's turn \n", current->name );

      /* Reset the color back to white */
      printReset();

      /* If the player makes a valid move */
      if ( make_move( current, board ) == TRUE )
      {
         /* Calculate scores */
         current->score = game_score( board, current->token );
         other->score = game_score( board, other->token );

         /* After each player has had their turn, switch the play order */
         swap_players( &current, &other );
      }
      else
      {
         break;
      }
   }

   /* Calculate Winner and assign to winner Pointer */
   if ( current->score > other->score )
   {
      winner = current;
   }
   else if ( other->score > current->score )
   {
      winner = other;
   }
   else 
   {
      printf("DRAW BETWEEN PLAYERS \n");
      winner = NULL;
   }

   return winner;
}

/**
 * does the work of applying the move requested by the user to the game board.
 * It iterates over all the directions from the coordinate specified to see
 * whether there are any pieces that can be captured. If there are no pieces
 * that can be captured in any direction, it is an invalid move.
 **/
BOOLEAN apply_move(game_board board, unsigned y, unsigned x,
                   enum cell player_token)
{
   /* Temporary variable which counts the number of pieces captured in each
    * for loop */
   unsigned captured_pieces = 0;

   /* Used to show the total number of pieces captured */
   int totalCapturedPieces = 0;

   /* Stores the starting coordinates for checking */
   unsigned startX = x;
   unsigned startY = y;

   /* Stores the values required to move a single cell in the current
   * direction */
   int xMovement = 0;
   int yMovement = 0;

   /* Used to control the direction of movement on the gameboard, and the 
    * for loop */
   int directionNo; 

   if ( board[ startX ][ startY ] != BLANK )
   {
      printf("Current cell is occupied \n");
      return FALSE;
   }

   /* There are 8 different directions this simply iterates over each 
   * direction and will NEVER change */
   for (directionNo = 0; directionNo < 8; directionNo++)  
   {
      captured_pieces = 0;

      /* Return x and y back to the starting position */
      x = startX;
      y = startY;

      /* Set the direction to be checked. NOTE directionNo is passed as the 
       * direction and converted through the enum direction */
      setDirection( directionNo, &xMovement, &yMovement);

      /* Move one space away from the starting position. PLEASE NOTE THAT
      * The y direction is reversed IE negative instead of positive, as
      * NORTH in the array is 0 */
      x = x + xMovement;
      y = y - yMovement;

      /* While the x + y values are within the boundaries of the board */
      while ( x <= BOARD_WIDTH && x >= 0 && y <= BOARD_HEIGHT && y >= 0 )
      {
         /* ---=== DETECTION LOGIC ===--- */
         /* Invalid Move - No pieces captured */
         if ( board[ x ][ y ] == player_token && captured_pieces < 1 )
         {
            break;
         }
         /* Invalid Move - Blank Square detected */
         else if ( board[ x ][ y ] == BLANK )
         {
            break;
         }
         else if ( board[ x ][ y ] != player_token )
         {
            /* Temporarily stores the number of pieces captured in the
            * current direction */
            captured_pieces++;              

            /* Move one cell forwards in the current direction */
            x = x + xMovement;
            y = y - yMovement;
         }
         /* Successfully completed CAPTURE condition */
         else if ( board[ x ][ y ] == player_token && captured_pieces >= 1 )
         {
            /* Increase the total number of captured pieces */
            totalCapturedPieces = captured_pieces;

            /* Now that a we know there are pieces to capture, move back to 
             * the start position and capture them as we go */
            while ( captured_pieces > 0 )
            {
               x = x - xMovement;
               y = y + yMovement;

               /* Move one cell backwards in the current direction */
               board[ x ][ y ] = player_token;
               captured_pieces--;
            }
            break;
         }
      }
      
   } /* End of directional for loop */
    

   if ( totalCapturedPieces > 0 )
   {
      /* If the move was successful place a player token at original
       * coordinates the player entered */
      board[ startX ][ startY ] = player_token;

      printf("\n");
      printf("This move captured %d Pieces \n", totalCapturedPieces);
      printf("\n");

      return TRUE;
   }

   /* If there were no matches, then this will be executed */
   printf("Invalid Move \n");
   return FALSE;
}


/**
 * simply count up how many locations contain the player_token 
 * specified on the game_board.
 **/
unsigned game_score(game_board board, enum cell player_token)
{
   int x, y, count = 0;

   for ( y = 0; y < BOARD_HEIGHT; y++ )
   {
      for ( x = 0; x < BOARD_WIDTH; x++ )
      {
         if ( board[x][y] == player_token )
         {
            count++;
         }
      }
   }
   return count;          
}


/**
 * swap the two player pointers passed in so that first points to the player
 * pointer that originally contained the second player and vice versa.
 **/
/* ### This was altered to be current and other ### */
void swap_players(struct player ** current, struct player ** other)
{
   /* Create a temporary struct to help switch the structs */
   struct player* temp = *current;

   *current = *other;
   *other   = temp;
}


/* This function is used to set the x and y movement values which are used 
 * to search through the gameboard for potential matches */
void setDirection( enum direction dir, int * xMovement, int * yMovement )
{
   /* direction enum is in the same order as shown in switch statement */
   switch(dir)
   {
      case NORTH:
      {
         *xMovement =  0;
         *yMovement =  1;
         break;
      }
      case NORTH_EAST:
      {
         *xMovement =  1;
         *yMovement =  1;
         break;
      }
      case EAST:
      {
         *xMovement =  1;
         *yMovement =  0;
         break;
      }
      case SOUTH_EAST:
      {
         *xMovement =  1;
         *yMovement = -1;
         break;
      }
      case SOUTH:
      {
         *xMovement =  0;
         *yMovement = -1;
         break;
      }
      case SOUTH_WEST:
      {
         *xMovement = -1;
         *yMovement = -1;
         break;
      }
      case WEST:
      {
         *xMovement = -1;
         *yMovement =  0;
         break;
      }
      case NORTH_WEST:
      {
         *xMovement = -1;
         *yMovement =  1;
      }
   }
} /* End of void setDirection() */
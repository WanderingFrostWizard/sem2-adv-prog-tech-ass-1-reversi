/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #1 
 * Full Name        : Cameron Watt
 * Student Number   : s3589163
 * Course Code      : COSC1076 - Advanced Programming Techniques
 * Program Code     : BP215 - Games and Graphics Programming
 * Start up code provided by Paul Miller 
 **********************************************************************/

 This file is for you to provide any extra information that your
 markers may find useful. For example. Bugs, inconsistencies, incomplete
 functionality, reasoning for design choices etc.


###################################################################
### ### NOTES ### ###

#define NEWLINE '\n' (utility.h) - I did not require this macro  



### TARGETS TO FIX ###

# Input Statements still returning extra newline char when user presses enter, but NOT with ctrl D



###################################################################
### ### FINISHED ### ###

# SCOREBOARD  - Initializes, sorts + adds successfully
# GAMEBOARD   - Displays Correctly + Colors
# Scoreboard adjusted, using printf  with a set amount of characters IE NAMELEN
# Initial turn DOES NOT NEED TO DISPLAY SCORES of 2 and 2 - BUT MAY BE CORRECTED
# Game Logic
# Coordinates VALIDATE and CAPTURE in a ALL directions
# TAB WIDTH TO 3



NAME input tested with 
- any combination of characters upto Max Characters (20) will be accepted
    but anything over the max character limit will be rejected and return to
    the main menu

- ENTER         -> WILL  Exit to main menu
- CTRL + D      -> WILL  Exit to main menu



MAIN MENU Input tested with 

    ### VALID ###
- numeric ( 1 - 3 )

    ### NON VALID ###
- numeric
- numeric ( above range ie 9 )
- non alpha-numeric
- ENTER         
- CTRL + D      -> WILL  Exit program 



Coordinate input tested with  
    ### VALID ###
- numeric ( 1 - 8 )

    ### NON VALID ###
- non alpha-numeric
- numeric ( above range ie 4, 5, 6, 7, 8, 9 )
- Not enough characters 
- ,,, ( against segmentation faults )
- A,A
- 9,9
- A,1
- 1,A
- 11,1
- 1,11
- 1.1
- 111
- AAA
- ENTER         -> WILL  Exit to main menu
- CTRL + D      -> WILL  Exit to main menu




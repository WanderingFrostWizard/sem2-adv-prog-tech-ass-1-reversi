/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #1 
 * Full Name        : Cameron Watt
 * Student Number   : s3589163
 * Course Code      : COSC1076 - Advanced Programming Techniques
 * Program Code     : BP215 - Games and Graphics Programming
 * Start up code provided by Paul Miller 
 **********************************************************************/
#include "shared.h"
#include "player.h"
#include "game.h"
#include "scoreboard.h"
#include "utility.h"

#ifndef REVERSI_H
#define REVERSI_H

void displayMenu();
int getInt( char* menuInput ); 

#endif

/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #1 
 * Full Name        : Cameron Watt
 * Student Number   : s3589163
 * Course Code      : COSC1076 - Advanced Programming Techniques
 * Program Code     : BP215 - Games and Graphics Programming
 * Start up code provided by Paul Miller 
 **********************************************************************/

#include "scoreboard.h"

/**
 * initalise the scoreboard so that the scores for each element of the array
 * is set to 0.
 **/
void init_scoreboard(score scores[MAX_SCORES])
{
   int i;

   for (i = 0; i < MAX_SCORES; i++)
   {
      strcpy(scores[i].name, " ");
      scores[i].token = BLANK;
      scores[i].score = 0;
   }
}


/**
 * insert the top scorer from the last game played in sorted order according
 * to their score 
 **/
BOOLEAN add_to_scoreboard(score scores[MAX_SCORES], struct player * winner)
{
   int position;

   if ( winner == NULL )
   {
      return FALSE;
   }

   /* Check to see if the winners score is high enough to make the scoreboard*/
   if ( winner->score > scores[MAX_SCORES - 1].score )
   {
      /* Starts at 10   - 1 to suit array and -1 as we have already checked 
       * scores[9] (last place) in the if statement above */
      for ( position = MAX_SCORES - 2; position > -1; position-- )
      {
         /* If the winners score is greater than the current position, move
          * that score down one line */
         if ( winner->score > scores[position].score )
         {
            strcpy(scores[position + 1].name, scores[position].name);
            scores[position + 1].token = scores[position].token;
            scores[position + 1].score = scores[position].score;  

            /* If the winners score is higher than first place */
            if ( position == 0 )
            {
               strcpy(scores[position].name, winner->name);
               scores[position].token = winner->token;
               scores[position].score = winner->score;
               printf("Congratulations %s on a new HIGH SCORE \n", 
                                                winner->name );
            }
         }
         /* If the winners score is NOT greater than the current position
          * being checked, then assign the winner to the previous position */
         else
         {
            strcpy(scores[position + 1].name, winner->name);
            scores[position + 1].token = winner->token;
            scores[position + 1].score = winner->score;

            printf("Congratulations %s you placed %d\n", winner->name,
                                                position + 1 );
            break;
         }

      } /* End of FOR LOOP */

      /* Extra Space before the menu reprints */
      printf("\n ");
      return TRUE;
   }
   else 
   {
      printf("Sorry %s your score didn't make the high scoreboard \n \n",
                                                winner->name);
      return FALSE;
   }

}

/**
 * display the scores in the scoreboard according to their order. Your output
 * should match that provided in the assignment specification.
 **/
void display_scores(score scores[MAX_SCORES])
{
   int i;

   printf( "\n" );
   repeatCharacter( '=', LINELEN );
   printf( "\t \t TODAYS HIGH SCORES\n" );
   repeatCharacter( '-', LINELEN );
   printf( "    | %-*s | %-*s |\n", NAMELEN, "Name ", 10, "Score " );
   repeatCharacter( '-', LINELEN );

   for ( i = 0; i < MAX_SCORES; i++ )
   {
      /* Print only entries that are not blank */
      if ( scores[i].score > 0 )
      {
         /* Check the players token color and print their score in that color*/
         if ( scores[i].token == BLUE )
         {
            printf( "%s", COLOR_BLUE );
         }
         else if ( scores[i].token == RED)
         {
            printf("%s", COLOR_RED);
         }
         /* Prints the position, the players name and the players score */
         printf( " %-2d | %-*s | %-*d |\n", (i + 1), NAMELEN, scores[i].name, 
                                             10, scores[i].score );

         /* Reset the color */
         printf( "%s", COLOR_RESET );
      }   
   }
   repeatCharacter( '-', LINELEN );
}

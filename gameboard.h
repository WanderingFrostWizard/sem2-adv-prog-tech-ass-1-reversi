/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #1 
 * Full Name        : Cameron Watt
 * Student Number   : s3589163
 * Course Code      : COSC1076 - Advanced Programming Techniques
 * Program Code     : BP215 - Games and Graphics Programming
 * Start up code provided by Paul Miller 
 **********************************************************************/
#include "shared.h"
#ifndef GAMEBOARD_H
#define GAMEBOARD_H

/* forwards declaration of a struct player. This means that the compiler knows
 * that such a datastructure will be available but it does not what it contains
 * and so we can have pointers to a player but we don't know how big it is or 
 * what it contains.
 */
struct player;

/* all the directions that we can capture pieces in leaving from this direction
 * To make this less confusing I have changed them from the originals to 
 * resemble a clockface, with NORTH as 0.  Original Version was ...
 * NORTH, SOUTH, EAST, WEST, NORTH_EAST, NORTH_WEST, SOUTH_EAST, SOUTH_WEST
 */ 
enum direction
{
    NORTH, NORTH_EAST, EAST, SOUTH_EAST, SOUTH, SOUTH_WEST, WEST, NORTH_WEST
};

/* how much space is required to display a column on the game board ? */
#define COLUMN_WIDTH 4
/* how tall is the game board - 8 places high */
#define BOARD_HEIGHT 8
/* how wide is the game board ? */
#define BOARD_WIDTH BOARD_HEIGHT 

/* Used to printout the gameboard (IE no of Cells * Width of Cells + Width of
 * Y index column */
#define BOARD_DIVIDER_WIDTH ( BOARD_WIDTH * COLUMN_WIDTH + COLUMN_WIDTH )

/* type definition of a game_board. It is just a 2-dimensional array */
typedef enum cell game_board[ BOARD_HEIGHT ][ BOARD_WIDTH ];

void init_game_board( game_board board );
void display_board( game_board board, struct player * human,
                   struct player * computer );

void printPlayerScores ( struct player* current, char numberText[4] );

/* Functions to print the gameboard */
void printXLabels ( int x );
void printCellValues ( game_board board, int x, int y );

#endif /* ifndef GAMEBOARD_H */

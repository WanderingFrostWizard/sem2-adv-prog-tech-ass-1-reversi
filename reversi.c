/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #1 
 * Full Name        : Cameron Watt
 * Student Number   : s3589163
 * Course Code      : COSC1076 - Advanced Programming Techniques
 * Program Code     : BP215 - Games and Graphics Programming
 * Start up code provided by Paul Miller 
 **********************************************************************/

#include "reversi.h"

/* Needed to add this to allow access to the utility functions */
#include "utility.c"


/**
 * the heart of the program. The main() function in particular should 
 * manage the main menu for the program.
 **/
int main(void)
{
   /* Declare the scoreboard array to be used throughout the game */
   score scrboard[MAX_SCORES];

   /* Declare player structs to handle the two players and temporarily store the 
    * winner of a game */
   struct player first, second, *winner = NULL;

   /* Array to store the user input and end of Line Character and NULL*/
   char menuInput[3];

   /* Stores the players main menu choice */
   int menuChoice = 0;

   /* Reset the colour of the unix text */
   printf( "%s", COLOR_RESET ); 

   /* initialise the scoreboard */
   init_scoreboard( scrboard );    

   /* Start the main menu, and repeat, until the user selects quit */
   while( menuChoice != 3 )            
   {
      displayMenu();

      /* Take in one character for the user's menu choice */
      menuChoice = getInt( menuInput );

      /* Play a game, then add the winner to the scoreboard */    
      switch( menuChoice )
      {
         /* Play Game */
         case 1:
         {
            /* Call the play game function, and then store the winner */ 
            winner = play_game( &first, &second ); 

            /* Add the winner to the scoreboard */
            add_to_scoreboard( scrboard, winner );
            break;
         }
         /* Display Scores */
         case 2:
         {
            display_scores( scrboard );
            break;         
         }
         /* Quit the program - The menu loop will exit as menuChoice == 3 */
         case 3:
         {
            printf( " -------------------------- \n" );
            printf( " |   Thanks for Playing   | \n" );
            printf( " -------------------------- \n" );
            printf( "\n");
            break;
         }
         default:
         {
            printf ( " Invalid Input Detected \n" );
         }
      }

   } /* End of Main Menu while loop */

   return EXIT_SUCCESS;
}


void displayMenu()
{
   printf ( " \n"); 
   printf ( " Welcome to Reversi! \n" ); 
   printf ( " =================== \n" );
   printf ( " Please select an Option: \n" );
   printf ( " 1. Play a Game \n" );
   printf ( " 2. Display High Scores \n" );
   printf ( " 3. Quit the program \n" );
   printf ( " Please enter your choice: \n" );
   printf ( " ->"); 
}


/* Receives a single character from the user to pass to the program menu */
int getInt( char* menuInput )
{
   /* 3 characters allows for the menu input + terminating characters /n, /0 */     
   if ( fgets( menuInput, 1 + EXTRACHARS, stdin ) == FALSE )
   {
      /* User wants to quit, so return a 3 to replicate user selecting exit 
       * program. IE when the 3 returns to the main menu, the game will exit */
      printf("\n");
      return 3;
   }  
   else 
   {
      /* Check to see if the user has entered MORE THAN 1 character */
      if ( menuInput[ strlen( menuInput ) - 1  ] != '\n')    
      {
         /* Clear the rest of the buffer 
         menuInput[1] = '\0';*/
         read_rest_of_line();
      }       
      else if ( menuInput[0] != '\n' )
      {
         /* Replace the Newline character with a NULL character */
         menuInput[1] = '\0';
         return (int) strtol( menuInput, NULL, 10 );
      }
      return 0;
   }  
}
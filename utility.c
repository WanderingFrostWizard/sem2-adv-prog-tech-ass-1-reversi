/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #1 
 * Full Name        : Cameron Watt
 * Student Number   : s3589163
 * Course Code      : COSC1076 - Advanced Programming Techniques
 * Program Code     : BP215 - Games and Graphics Programming
 * Start up code provided by Paul Miller 
 **********************************************************************/

#include "utility.h"

/**
 * function required to be used when clearing the buffer. It simply reads
 * each char from the buffer until the buffer is empty again. Please refer
 * to the materials on string and buffer handling in the course for more 
 * information.
 **/
void read_rest_of_line(void)
{
    int ch;
    while(ch = getc(stdin), ch != EOF && ch != NEWLINE)
        ;
    clearerr(stdin);
}

/* This function is used to repeat characters horizontally across the page 
 * which is generally the value LINELEN, but not always */
void repeatCharacter( char character, int numberOfRepeats )
{
   int repeat;   

   for (repeat = 0; repeat < numberOfRepeats; repeat++)
   {
      printf("%c", character);
   }
   printf("\n");
}

/* Set color functions are called when color needs to be changed */
void printRed()
{
   printf("%s", COLOR_RED);
}

void printBlue()
{
   printf("%s", COLOR_BLUE);
}

void printReset()
{
   printf("%s", COLOR_RESET);
}